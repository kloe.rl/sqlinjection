# SOLUTIONS 

## Injections SQL

``' OR 1=1--`` à la place du Username 



````
Login successful!
id: 1 - Username: testuser - Password: password123
id: 2 - Username: admin - Password: adminpass
````

## Technique utilisée

Une requête préparée

Ce qui a été modifié :

```php
$stmt = $conn->prepare("SELECT * FROM users where username = ? AND password = ?");
$stmt->execute([$user, $pass]);
```

## Autres bonnes pratiques

Utiliser l'extension **mysqli** qui permet de protéger une commande SQL de la présence de caractères spéciaux.