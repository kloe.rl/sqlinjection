<?php
$db_file = '/var/www/html/database.db';

try {
    $conn = new PDO("sqlite:$db_file");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $user = $_POST['username'];
    $pass = $_POST['password'];

    $stmt = $conn->prepare("SELECT * FROM users where username = ? AND password = ?");
    $stmt->execute([$user, $pass]);

    if ($stmt === false) {
        echo "Error executing the query.<br>";
    } else {
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $rowCount = count($rows);

        if ($rowCount > 0) {
            echo "Login successful!<br>";
            foreach ($rows as $row) {
                echo "id: " . $row["id"]. " - Username: " . $row["username"]. " - Password: " . $row["password"]. "<br>";
            }
        } else {
            echo "Incorrect username or password.";
        }
    }
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}
